package com.way.mat.manualworker.utils;

import java.util.Random;

public class RandomUtils {
    public static int randInt(int min, int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }
}
