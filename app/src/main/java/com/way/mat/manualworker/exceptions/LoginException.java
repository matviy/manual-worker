package com.way.mat.manualworker.exceptions;

public class LoginException extends Exception {
    public LoginException(String detailMessage) {
        super(detailMessage);
    }
}
