package com.way.mat.manualworker.exceptions;

public class InWarException extends Exception {
    private String warid;

    public InWarException(String warid) {
        this.warid = warid;
    }

    public String getWarid() {
        return this.warid;
    }
}
