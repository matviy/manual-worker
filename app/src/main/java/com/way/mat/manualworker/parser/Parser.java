package com.way.mat.manualworker.parser;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.Response;
import com.way.mat.manualworker.App;
import com.way.mat.manualworker.R;
import com.way.mat.manualworker.exceptions.CaptchaNotFoundException;
import com.way.mat.manualworker.exceptions.InWarException;
import com.way.mat.manualworker.exceptions.LoginException;
import com.way.mat.manualworker.exceptions.UnexpectedException;
import com.way.mat.manualworker.models.User;
import com.way.mat.manualworker.utils.RandomUtils;
import com.way.mat.manualworker.utils.RegexParser;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
    private static final String DOMAIN = "http://www.lordswm.com/";
    private static final String FLASH_DOMAIN = "http://dcdn.lordswm.com/";
    private static final String EMPLOYER_1 = "mn";
    private static final String EMPLOYER_2 = "fc";
    private static final String EMPLOYER_3 = "sh";
    private CaptchaForm captchaForm;
    private Client client = new Client();
    private User user;

    public static Parser create(User user) {
        Parser parser = new Parser();
        parser.user = user;
        return parser;
    }

    public static String getGlobalInfo(String userId) throws IOException {
        return new Client().get("http://www.lordswm.com/pl_info.php?id=" + userId).body().string();
    }

    public static boolean isInThisBattle(String userId, String warId) throws IOException {
        String responseBody = getGlobalInfo(userId);
        return responseBody.contains("Character is in combat") && responseBody.contains("warlog.php?warid=" + warId);
    }

    public static boolean isOffline(String userId) throws IOException {
        return getGlobalInfo(userId).contains("Character went offline");
    }


    public boolean login() throws LoginException, IOException, UnexpectedException, InWarException {
        Response response = this.client.post("http://www.lordswm.com/login.php", new FormEncodingBuilder().add("LOGIN_redirect", "1").add("lreseted", "1").add("preseted", "1").add("x", "0").add("y", "0").add("pliv", "1").addEncoded("login", URLEncoder.encode(this.user.getUsername(), "CP1251")).addEncoded("pass", URLEncoder.encode(this.user.getPassword(), "CP1251")).build());
        String responseBody = response.body().string();
        Response priorResponse = response.priorResponse();
        if (priorResponse != null) {
            String location = priorResponse.header("Location");
            if (location == null) {
                throw new UnexpectedException(RegexParser.getTitle(responseBody));
            } else if (location.startsWith("war.php")) {
                throw new InWarException(RegexParser.getWarId(location));
            } else if (location.equals("home.php")) {
                return true;
            } else {
                throw new UnexpectedException(RegexParser.getTitle(responseBody));
            }
        } else if (responseBody.contains("Wrong user name or password.")) {
            throw new LoginException(App.getInstance().getString(R.string.login_error_wrong_user_or_password));
        } else if (responseBody.contains("Incorrect characters from picture code. Please try again.")) {
            throw new LoginException(App.getInstance().getString(R.string.login_using_browser));
        } else if (responseBody.contains("Hero " + this.user.getUsername() + " is blocked.")) {
            throw new LoginException(App.getInstance().getString(R.string.login_error_user_blocked));
        } else {
            throw new UnexpectedException(RegexParser.getTitle(responseBody));
        }
    }

    public boolean isNotWorking() throws IOException {
        return this.client.get("http://www.lordswm.com/home.php").body().string().contains("You are currently unemployed");
    }

    public String getUserId() throws IOException {
        return RegexParser.getUserId(this.client.get("http://www.lordswm.com/home.php").body().string());
    }

    public void logout() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    Parser.this.client.get("http://www.lordswm.com/logout.php?" + String.valueOf(RandomUtils.randInt(11111, 99999)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    protected Set<String> getEmployers(String group) throws IOException {
        String responseBody = this.client.get("http://www.lordswm.com/map.php?st=" + group).body().string();
        Set<String> result = new LinkedHashSet();
        Matcher m = Pattern.compile("<a href='(object-info\\.php\\?id=[\\d]*)' style='text-decoration:none;'>&raquo;&raquo;&raquo;</a>").matcher(responseBody);
        while (m.find()) {
            result.add(m.group(1));
        }
        return result;
    }

    public String getObjectId(String object) throws IOException {
        String result = "";
        Matcher m = Pattern.compile("object-info\\.php\\?id=([\\d]*)").matcher(object);
        if (m.find()) {
            result = m.group(1);
        }
        return result;
    }

    public Set<String> getAllEmployers() throws IOException {
        Set<String> result = new LinkedHashSet();
        result.addAll(getEmployers(EMPLOYER_3));
        result.addAll(getEmployers(EMPLOYER_2));
        result.addAll(getEmployers(EMPLOYER_1));
        return result;
    }

    public String getDeviceIDs() throws IOException {
        return this.client.get("https://bitbucket.org/matviy/manual-worker/overview").body().string();
    }

    public CaptchaForm getCaptchaUrl(String employer) throws IOException, CaptchaNotFoundException {
        this.captchaForm = CaptchaForm.create(this.client.get(DOMAIN + employer).body().string());
        this.captchaForm.src = DOMAIN + this.captchaForm.src;
        this.captchaForm.swf = FLASH_DOMAIN + this.captchaForm.src;
        return this.captchaForm;
    }

    public boolean apply(String captcha) throws IOException {
        return this.client.get("http://www.lordswm.com/object_do.php?id=" + this.captchaForm.id + "&code=" + captcha + "&code_id=" + this.captchaForm.code_id + "&pl_id=" + this.captchaForm.pl_id).body().string().contains("You have successfully enrolled.");
    }

    public boolean isHighThreatLevel() throws IOException {
        return this.client.get("http://www.lordswm.com/map.php").body().string().contains("High\" alt=\"Threat level:");
    }

    public void skipHunt() throws IOException {
        this.client.get("http://www.lordswm.com/map.php?action=skip");
    }

    public User getUser() {
        return user;
    }

    public CaptchaForm getCaptchaForm() {
        return this.captchaForm;
    }
}
