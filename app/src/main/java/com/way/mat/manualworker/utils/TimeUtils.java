package com.way.mat.manualworker.utils;

import java.util.Calendar;

/**
 * Created by mpodolsky on 13.10.2015.
 */
public class TimeUtils {

    public static String getTimeString(long time) {
        String strTime = null;
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);
        int minutes = c.get(Calendar.MINUTE);
        String minutesStr = minutes >= 10 ? String.valueOf(minutes) : "0" + String.valueOf(minutes);
        int hours = c.get(Calendar.HOUR_OF_DAY);
        String hoursStr = hours >= 10 ? String.valueOf(hours) : "0" + String.valueOf(hours);
        strTime = hoursStr + ":" + minutesStr;
        return strTime;
    }

    public static boolean isNightTime(long time) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);
        int hourOfDay = c.get(Calendar.HOUR_OF_DAY);
        return hourOfDay > 0 && hourOfDay < 8;
    }

}
