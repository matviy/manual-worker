package com.way.mat.manualworker.exceptions;

public class AntigateException extends Exception {
    public AntigateException(String detailMessage) {
        super(detailMessage);
    }
}
