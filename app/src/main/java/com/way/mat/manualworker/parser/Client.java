package com.way.mat.manualworker.parser;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request.Builder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

public class Client extends OkHttpClient {
    static final String USER_AGENT = "Mozilla/5.0 (Windows; U; Windows NT 6.0; ru; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3";

    public Client() {
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        setCookieHandler(cookieManager);
        setFollowRedirects(true);
        setConnectTimeout(30, TimeUnit.SECONDS);
        setReadTimeout(30, TimeUnit.SECONDS);
    }

    public Response get(String url) throws IOException {
        Response response = newCall(new Builder().addHeader("User-Agent", USER_AGENT).addHeader("Connection", "close").url(url).get().build()).execute();
        if (response.isSuccessful()) {
            return response;
        }
        throw new IOException("Unexpected code " + response);
    }

    public Response post(String url, RequestBody formBody) throws IOException {
        Response response = newCall(new Builder().addHeader("User-Agent", USER_AGENT).addHeader("Connection", "close").addHeader("Content-Type", "application/x-www-form-urlencoded").addHeader("Accept-Language", "en-US,en;q=0.8,ru;q=0.6").url(url).post(formBody).build()).execute();
        if (response.isSuccessful()) {
            return response;
        }
        throw new IOException("Unexpected code " + response);
    }
}
