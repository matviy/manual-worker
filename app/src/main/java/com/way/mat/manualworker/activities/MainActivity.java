package com.way.mat.manualworker.activities;

import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.way.mat.manualworker.R;
import com.way.mat.manualworker.listeners.LoadListener;
import com.way.mat.manualworker.models.User;
import com.way.mat.manualworker.parser.Parser;
import com.way.mat.manualworker.tasks.CheckDeviceTask;
import com.way.mat.manualworker.tasks.FindEmployerTask;
import com.way.mat.manualworker.utils.Constants;
import com.way.mat.manualworker.utils.PreferencesManager;
import com.way.mat.manualworker.utils.RandomUtils;

import java.io.UnsupportedEncodingException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements FindEmployerTask.EmployerFoundCallback, CheckDeviceTask.DeviceCheckedCallback {

    @Bind(R.id.webview)
    WebView mContentView;
    @Bind(R.id.progress)
    ProgressBar mProgress;
    @Bind(R.id.text)
    TextView status;
    @Bind(R.id.name)
    EditText etName;
    @Bind(R.id.work)
    Button btnWork;
    @Bind(R.id.enroll)
    Button btnEnroll;
    @Bind(R.id.logout)
    Button btnLogout;

    String objectUrl = "";

    private boolean isLoginning = false;
    private boolean isLoadingHome = false;
    private boolean isLoadingObject = false;
    private boolean isApplying = false;

    private Parser parser = null;

    Handler mHandler = new Handler();
    Handler mSecondHandler = new Handler();
    Runnable mRunnable;

    //    String username = "cpur";
    String password = "1asdsasd";
    String cap = "";
    String lastUrl = "";

    private String test;
//    String username = "Samurai Jack";
//    String password = "1asdsasd";

    private PowerManager.WakeLock mWakeLock;
    private WifiManager.WifiLock mWifiLock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        window.addFlags(WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        ButterKnife.bind(this);

        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Toast.makeText(MainActivity.this, android_id, Toast.LENGTH_SHORT).show();

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//                try {
//                    test = Parser.create(null).getDeviceIDs();
//                    int a = 5;
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();

        setUpWebView(mContentView);

        mRunnable = new Runnable() {
            @Override
            public void run() {
                findWork();
            }
        };


        Intent intent = getIntent();
        if (intent != null && intent.getIntExtra(Constants.AUTOSTART, 0) == 1) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    findWork();
                }
            }, 3000);
        } else {
            try {
                new CheckDeviceTask(Parser.create(new User("test", "test")), null, this).execute(android_id);
            } catch (User.UserExistsException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected void onDestroy() {
        if (mWakeLock != null && mWakeLock.isHeld()) {
            mWakeLock.release();
        }
        if (mWifiLock != null && mWifiLock.isHeld()) {
            mWifiLock.release();
        }
        super.onDestroy();
    }

    @OnClick(R.id.logout)
    public void logout() {
        mProgress.setVisibility(View.VISIBLE);
        mContentView.loadUrl(Constants.DOMAIN + "logout.php?" + String.valueOf(RandomUtils.randInt(11111, 99999)));
    }

    @OnClick(R.id.work)
    public void work() {
        findWork();
    }

    @OnClick(R.id.enroll)
    public void enroll() {
        clickOnEnroll();
    }

    private void setUpWebView(WebView webView) {
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setDomStorageEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setBuiltInZoomControls(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        webView.addJavascriptInterface(new LoadListener(new LoadListener.Callback() {
            @Override
            public void onObjectIdObtained(String id) {
                if (!TextUtils.isEmpty(id)) {
                    mContentView.loadUrl("javascript:document.getElementById('" + id + "').scrollIntoView();");
                    final Handler h1 = new Handler();
                    h1.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            clickOnCaptchaField();
                        }
                    }, 1000);
                } else {
                    Toast.makeText(MainActivity.this, "Empty html id", Toast.LENGTH_SHORT).show();
                    restart(Constants.TIME_TEN_SECONDS);
                }
            }
        }), "HTMLOUT");

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                mProgress.setVisibility(View.GONE);
                if (isLoginning) {
                    isLoginning = false;
                    isLoadingHome = true;
                    view.loadUrl(Constants.DOMAIN + "home.php");
                } else if (isLoadingHome) {
                    isLoadingObject = true;
                    isLoadingHome = false;
                    view.loadUrl(Constants.DOMAIN + objectUrl);
                } else if (isLoadingObject) {
//                    isLoadingObject = false;
                    if (isApplying) {
                        view.setInitialScale(100);
                        view.scrollTo(0, 0);
                    } else {
                        view.setInitialScale(180);
                        view.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
                    }
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url); //this is controversial - see comments and other answers
                return true;
            }

        });

    }

    private void loadData() {
        mProgress.setVisibility(View.VISIBLE);
        mContentView.loadUrl(Constants.DOMAIN + "home.php");
    }

    private void findWork() {
        isLoginning = false;
        isLoadingObject = false;
        isApplying = false;
        etName.clearFocus();
        mContentView.requestLayout();
        mContentView.requestFocus();
        mProgress.setVisibility(View.VISIBLE);
        mContentView.loadUrl(Constants.DOMAIN + "logout.php?" + String.valueOf(RandomUtils.randInt(11111, 99999)));

        parser = null;
        try {
            String username = etName.getText().toString();
            parser = Parser.create(new User(username, password));
        } catch (User.UserExistsException e) {

        }
        new FindEmployerTask(parser, status, this).execute();
    }

    private void loginn() {
        isLoginning = true;

        mProgress.setVisibility(View.VISIBLE);
        String username = etName.getText().toString();
        String data = "LOGIN_redirect=1&lreseted=1&preseted=1&x=0&y=0&pliv=1&login=" + username + "&pass=" + password;

        try {
            mContentView.postUrl(Constants.DOMAIN + "/login.php", data.getBytes("CP1251"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void clickOnCaptchaField() {
        // Obtain MotionEvent object
        mContentView.requestFocus();
        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis() + 100;
        float x = 500f;
        float y = 70f;
// List of meta states found here: developer.android.com/reference/android/view/KeyEvent.html#getMetaState()
        int metaState = 0;
        MotionEvent motionEvent1 = MotionEvent.obtain(
                downTime,
                eventTime,
                MotionEvent.ACTION_DOWN,
                x,
                y,
                metaState
        );

        MotionEvent motionEvent2 = MotionEvent.obtain(
                downTime,
                eventTime,
                MotionEvent.ACTION_UP,
                x,
                y,
                metaState
        );

        mContentView.dispatchTouchEvent(motionEvent1);
        mContentView.dispatchTouchEvent(motionEvent2);
    }

    private void clickOnEnroll() {
        // Obtain MotionEvent object
        mContentView.requestFocus();

        isApplying = true;

        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis() + 100;
        float x = 600f;
        float y = 70f;
// List of meta states found here: developer.android.com/reference/android/view/KeyEvent.html#getMetaState()
        int metaState = 0;
        MotionEvent motionEvent1 = MotionEvent.obtain(
                downTime,
                eventTime,
                MotionEvent.ACTION_DOWN,
                x,
                y,
                metaState
        );

        MotionEvent motionEvent2 = MotionEvent.obtain(
                downTime,
                eventTime,
                MotionEvent.ACTION_UP,
                x,
                y,
                metaState
        );

        mContentView.dispatchTouchEvent(motionEvent1);
        mContentView.dispatchTouchEvent(motionEvent2);

        new Thread(new Runnable() {
            @Override
            public void run() {
                mSecondHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        restart(Constants.TIME_TEN_SECONDS);
                    }
                });
            }
        }).start();
    }

    @Override
    public void employerFound(String str) {

        if (!TextUtils.isEmpty(str)) {
            switch (str) {
                case Constants.STATUS_NETWORK_ERROR:
                    status.setText(R.string.status_network_error);
                    //TODO restart
                    restart(Constants.TIME_TEN_SECONDS);
                    break;
                case Constants.STATUS_IN_WAR:
                    status.setText(R.string.status_in_war);
                    restart(Constants.TIME_TEN_MINUTES);
                    //TODO restart
                    break;
                case Constants.STATUS_LOGIN_UNEXPECTED_ERROR:
                    status.setText(R.string.status_login_unexpected_error);
                    restart(Constants.TIME_TEN_SECONDS);
                    //TODO restart
                    break;
                case Constants.STATUS_LOGIN_ERROR:
                    status.setText(R.string.status_login_error);
                    restart(Constants.TIME_TEN_SECONDS);
                    //TODO restart
                    break;
                case Constants.STATUS_CAPTCHA_ERROR:
                    status.setText(R.string.status_captcha_error);
                    restart(Constants.TIME_TEN_SECONDS);
                    //TODO restart
                    break;
                case Constants.STATUS_NO_EMPLOYERS:
                    status.setText(R.string.status_no_employers);
                    restart(Constants.TIME_TEN_SECONDS);
                    //TODO restart
                    break;
                case Constants.STATUS_WORKING_ALREADY:
                    status.setText(R.string.status_working_already);
//                    Toast.makeText(MainActivity.this, "already working", Toast.LENGTH_SHORT).show();
                    //TODO restart
                    break;
                default:
                    objectUrl = str;

                    if (parser != null && parser.getCaptchaForm() != null && !TextUtils.isEmpty(parser.getCaptchaForm().getSrc())) {
                        loginn();
                    } else {
                        //TODO restart
                        status.setText("Something wrong");
                        restart(Constants.TIME_TEN_SECONDS);
                    }
                    break;
            }
        }
    }

    @Override
    public void isWorkaholicAction(User user, Boolean bool) {
//        status.setTextColor(Color.parseColor("#ff0000"));
    }

    private void restart(final long delay) {
//        time.setText(TimeUtils.getTimeString(System.currentTimeMillis()) + " restart: " + ((float) delay) / 1000 / 60 + "m");
//        mHandler.postDelayed(mRunnable, delay);
    }

    @Override
    public void deviceChecked(boolean isInList) {
        if (isInList) {
            Toast.makeText(MainActivity.this, "you are in list!!!", Toast.LENGTH_SHORT).show();
            PreferencesManager.getInstance().setIsInList(isInList);
            btnEnroll.setEnabled(true);
            btnLogout.setEnabled(true);
            btnWork.setEnabled(true);
        } else {
            Toast.makeText(MainActivity.this, "not in list!!!", Toast.LENGTH_SHORT).show();
        }
    }
}
