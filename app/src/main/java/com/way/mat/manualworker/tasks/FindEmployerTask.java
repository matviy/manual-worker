package com.way.mat.manualworker.tasks;

import android.os.AsyncTask;
import android.widget.TextView;

import com.way.mat.manualworker.R;
import com.way.mat.manualworker.exceptions.CaptchaNotFoundException;
import com.way.mat.manualworker.exceptions.InWarException;
import com.way.mat.manualworker.exceptions.LoginException;
import com.way.mat.manualworker.exceptions.UnexpectedException;
import com.way.mat.manualworker.models.User;
import com.way.mat.manualworker.parser.CaptchaForm;
import com.way.mat.manualworker.parser.Parser;
import com.way.mat.manualworker.utils.Constants;
import com.way.mat.manualworker.utils.L;

import java.io.IOException;
import java.util.Set;

/**
 * Created by Alexander Rubanskiy on 09.10.2015.
 */

public class FindEmployerTask extends AsyncTask<Void, FindEmployerTask.Status, String> {

    public static final String TAG = "tester_find_e";

    protected EmployerFoundCallback callback;
    protected String loginErrorMessage;
    private Parser parser;
    private final TextView tv_status;

    public interface EmployerFoundCallback {
        void employerFound(String str);

        void isWorkaholicAction(User user, Boolean bool);
    }

    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$whyte624$heroeswm$ui$task$FindEmployerTask$Status = new int[Status.values().length];

        static {
            try {
                $SwitchMap$com$whyte624$heroeswm$ui$task$FindEmployerTask$Status[Status.LOGIN.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$whyte624$heroeswm$ui$task$FindEmployerTask$Status[Status.LOGIN_UNEXPECTED_ERROR.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$whyte624$heroeswm$ui$task$FindEmployerTask$Status[Status.LOGIN_ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$whyte624$heroeswm$ui$task$FindEmployerTask$Status[Status.WORKING_ALREADY.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$whyte624$heroeswm$ui$task$FindEmployerTask$Status[Status.FINDING_EMPLOYERS.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$whyte624$heroeswm$ui$task$FindEmployerTask$Status[Status.NO_EMPLOYERS.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$whyte624$heroeswm$ui$task$FindEmployerTask$Status[Status.FETCHING_CAPTCHA.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$whyte624$heroeswm$ui$task$FindEmployerTask$Status[Status.CAPTCHA_ERROR.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$whyte624$heroeswm$ui$task$FindEmployerTask$Status[Status.CAPTCHA_FOUND.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$whyte624$heroeswm$ui$task$FindEmployerTask$Status[Status.NETWORK_ERROR.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$whyte624$heroeswm$ui$task$FindEmployerTask$Status[Status.IN_WAR.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
        }
    }

    public enum Status {
        LOGIN,
        LOGIN_UNEXPECTED_ERROR,
        LOGIN_ERROR,
        WORKING_ALREADY,
        FINDING_EMPLOYERS,
        NO_EMPLOYERS,
        FETCHING_CAPTCHA,
        CAPTCHA_ERROR,
        CAPTCHA_FOUND,
        IN_WAR,
        NETWORK_ERROR
    }

    public FindEmployerTask(Parser parser, TextView tv_status, EmployerFoundCallback callback) {
        this.parser = parser;
        this.tv_status = tv_status;
        this.callback = callback;
    }

    protected String doInBackground(Void... params) {
        String str = null;
        try {
            publishProgress(new Status[]{Status.LOGIN});
            if (this.parser.login()) {
                String s = this.parser.getDeviceIDs();
                if (this.parser.isNotWorking()) {
                    L.d(TAG, "finding employers");
                    publishProgress(new Status[]{Status.FINDING_EMPLOYERS});
                    Set<String> employers = this.parser.getAllEmployers();
                    if (employers.size() > 0) {
                        for (String employer : employers) {
                            publishProgress(new Status[]{Status.FETCHING_CAPTCHA});
                            try {
                                CaptchaForm captchaForm = this.parser.getCaptchaUrl(employer);
                                this.callback.isWorkaholicAction(parser.getUser(), captchaForm.is_workaholic);
                                publishProgress(new Status[]{Status.CAPTCHA_FOUND});
//                                str = RegexParser.getObjectId(employer);
                                str = employer;
                                break;
                            } catch (CaptchaNotFoundException e) {
//                                String s = "";
                            }
                        }
                        if (str == null) {
                            publishProgress(new Status[]{Status.CAPTCHA_ERROR});
                            this.parser.logout();
                            return Constants.STATUS_CAPTCHA_ERROR;
                        }
                    } else {
                        L.d(TAG, "no employers");
                        publishProgress(new Status[]{Status.NO_EMPLOYERS});
                        this.parser.logout();
                        return Constants.STATUS_NO_EMPLOYERS;
                    }
                } else {
                    L.d(TAG, "already working");
                    publishProgress(new Status[]{Status.WORKING_ALREADY});
                    this.parser.logout();
                    return Constants.STATUS_WORKING_ALREADY;
                }
                return str;
            }
            publishProgress(new Status[]{Status.LOGIN_UNEXPECTED_ERROR});
            this.parser.logout();
            return Constants.STATUS_LOGIN_UNEXPECTED_ERROR;
        } catch (LoginException e2) {
            e2.printStackTrace();
            this.loginErrorMessage = e2.getMessage();
            L.d(TAG, "login error");
            publishProgress(new Status[]{Status.LOGIN_ERROR});
            this.parser.logout();
            return Constants.STATUS_LOGIN_ERROR;
        } catch (IOException e3) {
            e3.printStackTrace();
            L.d(TAG, "network error");
            publishProgress(new Status[]{Status.NETWORK_ERROR});
            this.parser.logout();
            return Constants.STATUS_NETWORK_ERROR;
        } catch (UnexpectedException e4) {
            L.d(TAG, "login unexpected error");
            publishProgress(new Status[]{Status.LOGIN_UNEXPECTED_ERROR});
            this.parser.logout();
            return Constants.STATUS_LOGIN_UNEXPECTED_ERROR;
        } catch (InWarException e5) {
            L.d(TAG, "in war");
            publishProgress(new Status[]{Status.IN_WAR});
            this.parser.logout();
            return Constants.STATUS_IN_WAR;
        }
    }

    protected void onProgressUpdate(Status... values) {
        Status status = values[0];
        if (this.tv_status != null) {
            this.tv_status.setText(status.toString());
            switch (AnonymousClass1.$SwitchMap$com$whyte624$heroeswm$ui$task$FindEmployerTask$Status[status.ordinal()]) {
                case R.styleable.View_android_focusable /*1*/:
                    this.tv_status.setText(R.string.status_login);
                    break;
                case R.styleable.View_paddingStart /*2*/:
                    this.tv_status.setText(R.string.status_login_unexpected_error);
                    break;
                case R.styleable.View_paddingEnd /*3*/:
                    this.tv_status.setText(this.loginErrorMessage);
                    break;
                case R.styleable.View_theme /*4*/:
                    this.tv_status.setText(R.string.status_working_already);
                    break;
                case R.styleable.Toolbar_contentInsetStart /*5*/:
                    this.tv_status.setText(R.string.status_finding_employers);
                    break;
                case R.styleable.Toolbar_contentInsetEnd /*6*/:
                    this.tv_status.setText(R.string.status_no_employers);
                    break;
                case R.styleable.Toolbar_contentInsetLeft /*7*/:
                    this.tv_status.setText(R.string.status_captcha);
                    break;
                case R.styleable.Toolbar_contentInsetRight /*8*/:
                    this.tv_status.setText(R.string.status_captcha_error);
                    break;
                case R.styleable.Toolbar_popupTheme /*9*/:
                    this.tv_status.setText(R.string.status_captcha_found);
                    break;
                case R.styleable.Toolbar_titleTextAppearance /*10*/:
                    this.tv_status.setText(R.string.status_network_error);
                    break;
                case R.styleable.Toolbar_subtitleTextAppearance /*11*/:
                    this.tv_status.setText(R.string.login_error_combat);
                    break;
            }
        }
        super.onProgressUpdate(values);
    }

    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (callback != null) {
            this.callback.employerFound(s);
        }
    }
}
