package com.way.mat.manualworker.utils;

import android.util.Log;

import com.way.mat.manualworker.BuildConfig;


/**
 * Created by mpodolsky on 22.10.2015.
 */
public class L {

    public static final int WARN = 0;
    public static final int INFORMATION = 0;
    public static final int DEBUG = 0;
    public static final int ERROR = 0;

    public static void d(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, msg);
        }
    }

}
