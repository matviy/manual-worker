package com.way.mat.manualworker.listeners;

import android.text.TextUtils;
import android.util.Log;
import android.webkit.JavascriptInterface;

import com.way.mat.manualworker.utils.RegexParser;


/**
 * Created by mpodolsky on 21.01.2016.
 */
public class LoadListener {

    public interface Callback {
        void onObjectIdObtained(String id);
    }

    private Callback mCallback;

    public LoadListener(Callback callback) {
        this.mCallback = callback;
    }

    @JavascriptInterface
    public void processHTML(String html)
    {
//        String url = RegexParser.getSwfUrl(html);
//        if (!TextUtils.isEmpty(url)) {
//            PreferencesManager.getInstance().setSwfUrl(Constants.DOMAIN + url);
//            Log.d("flash_test", "url: " + Constants.DOMAIN + url);
//        }
//
//        String id = RegexParser.getSwfCodeId(html);
//        if (!TextUtils.isEmpty(id)) {
//            PreferencesManager.getInstance().setSwfCodeId(id);
//            Log.d("flash_test", "id: " + id);
//        }

        if (!html.contains("free online game")) {
            String htmlid = RegexParser.getHtmlObjectId(html);
            if (!TextUtils.isEmpty(htmlid)) {
                Log.d("flash_test", "htmlid: " + htmlid);
            } else {
                Log.d("flash_test", "htmlid empty");
            }
            mCallback.onObjectIdObtained(htmlid);
        }
//        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();

    }

//    @JavascriptInterface
//    public void scrollPage(String is)
//    {
//        document.
//    }

}
