package com.way.mat.manualworker.models;


import com.way.mat.manualworker.BuildConfig;

public class User {

    public static final String USERNAME = "username";

    public Long _id;
    private String password;
    private String userId = BuildConfig.FLAVOR;
    private String username;
    private int position;
    private boolean isWorkaholic;

    public class UserExistsException extends Exception {
    }

    public User(String username, String password) throws UserExistsException {
        this.username = username;
        this.password = password;
    }

    public User() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isWorkaholic() {
        return isWorkaholic;
    }

    public void setIsWorkaholic(boolean isWorkaholic) {
        this.isWorkaholic = isWorkaholic;
    }

}
