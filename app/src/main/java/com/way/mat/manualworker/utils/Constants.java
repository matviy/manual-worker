package com.way.mat.manualworker.utils;

/**
 * Created by mpodolsky on 21.01.2016.
 */
public interface Constants {

    String DEFAULT_PASSWORD = "1asdsasd";

    long TIME_SECOND = 1000;
    long TIME_TEN_SECONDS = 10 * 1000;
    long TIME_THIRTY_SECONDS = 30 * 1000;
    long TIME_MINUTE = 1 * 60 * 1000;
    long TIME_FIVE_MINUTES = 5 * 60 * 1000;
    long TIME_TEN_MINUTES = 10 * 60 * 1000;
    long TIME_THIRTY_MINUTES = 30 * 60 * 1000;
    long TIME_HOUR = 60 * 60 * 1000;

    String DOMAIN = "http://www.lordswm.com/";
    String AUTOSTART = "autostart";

    //statuses
    String STATUS_WORKING_ALREADY = "status_already_working";
    String STATUS_NO_EMPLOYERS = "status_no_employers";
    String STATUS_CAPTCHA_ERROR = "status_captcha_error";
    String STATUS_LOGIN_UNEXPECTED_ERROR = "status_unexpected_login_error";
    String STATUS_LOGIN_ERROR = "status_login_error";
    String STATUS_IN_WAR = "status_in_war";
    String STATUS_NETWORK_ERROR = "status_network_error";

}
