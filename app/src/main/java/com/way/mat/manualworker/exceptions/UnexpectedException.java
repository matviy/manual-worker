package com.way.mat.manualworker.exceptions;

public class UnexpectedException extends Exception {
    public UnexpectedException(String detailMessage) {
        super(detailMessage);
    }
}
