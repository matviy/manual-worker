package com.way.mat.manualworker.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class
        PreferencesManager {

    private static final String PREF_NAME = "recycler_prefs";

    private static final String IN_LIST = "in_list";

    private static PreferencesManager sInstance;
    private final SharedPreferences mPref;

    private PreferencesManager(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PreferencesManager(context);
        }
    }

    public static synchronized PreferencesManager getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(PreferencesManager.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }
        return sInstance;
    }

    public SharedPreferences getPreferences() {
        return mPref;
    }

    public void remove(String key) {
        mPref.edit()
                .remove(key)
                .commit();
    }

    public boolean clear() {
        return mPref.edit()
                .clear()
                .commit();
    }

    public void setIsInList(boolean isInList) {
        mPref.edit()
                .putBoolean(IN_LIST, isInList)
                .commit();
    }

    public boolean isInList() {
        return mPref.getBoolean(IN_LIST, false);
    }

}
