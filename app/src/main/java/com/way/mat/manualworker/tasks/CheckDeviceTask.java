package com.way.mat.manualworker.tasks;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.widget.TextView;

import com.way.mat.manualworker.parser.Parser;

import java.io.IOException;

/**
 * Created by Alexander Rubanskiy on 09.10.2015.
 */

public class CheckDeviceTask extends AsyncTask<String, Void, Boolean> {

    public static final String TAG = "tester_check";

    protected DeviceCheckedCallback callback;
    protected String loginErrorMessage;
    private Parser parser;
    private final TextView tv_status;

    public interface DeviceCheckedCallback {
        void deviceChecked(boolean isInList);
    }

    public CheckDeviceTask(Parser parser, TextView tv_status, DeviceCheckedCallback callback) {
        this.parser = parser;
        this.tv_status = tv_status;
        this.callback = callback;
    }

    protected Boolean doInBackground(String... params) {
        String deviceID = params[0];
        if (!TextUtils.isEmpty(deviceID)) {
            try {
                String page = this.parser.getDeviceIDs();
                return page.toLowerCase().contains(deviceID.toLowerCase());
            } catch (IOException e) {
                return false;
            }
        } else {
            return false;
        }
    }

    protected void onPostExecute(Boolean isInList) {
        super.onPostExecute(isInList);
        if (callback != null) {
            this.callback.deviceChecked(isInList);
        }
    }
}
