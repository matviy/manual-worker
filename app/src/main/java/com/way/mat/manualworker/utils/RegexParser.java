package com.way.mat.manualworker.utils;


import android.text.TextUtils;

import com.way.mat.manualworker.BuildConfig;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexParser {
    private static String REGEX_TITLE = "<title>([^<]*)</title>";
    private static String REGEX_USER_ID = "\"pl_hunter_stat\\.php\\?id=([\\d]*)\"";
    private static String REGEX_WAR_ID = "war\\.php\\?warid=([\\d]*)";
    private static String REGEX_GAME_ID = "cgame\\.php\\?gameid=([\\d]*)";
    private static String REGEX_USER_LEVEL = "combat level:\\s*([\\d]*)";
    private static String REGEX_USER_EXP = "combat level:\\s*([\\d]*)\\S*\\s(\\()(([\\d\\,])+)(\\))";
    private static String REGEX_USER_EXP_TO_NEXT_LEVEL = "combat level:\\s*\\S*\\s\\S*\\s[^//+]*[\\+]?(([\\d\\,])+)";
    private static String REGEX_USER_GOLD = "title=\"gold\"\\s?[^\\d]*(([\\d\\,])+)";
    private static String REGEX_REGISTER_CODE = "param=\\*english\\*([a-z0-9]+)\"";
    private static String REGEX_JOIN_ID = "(.*?)(join_to_card_game\\.php\\?id=)([\\d]+)";
    private static String PLAYER_IDENTIFCATOR = "pl_info.php?id=";
    private static String ACCEPT_IDENTIFCATOR = "acard_game.php?id=";
    private static String REGEX_ACCEPT_ID = "acard_game\\.php\\?id=([\\d]+)";
    private static String REGEX_ACCEPT_USERNAME = "pl_info\\.php\\?id=([\\d]+)'>([^<]+).*?joins your challenge";
    private static String CAN_ACCEPT = "joins your challenge";
    private static String WAITING_ACCEPT = "waiting";
    private static String IS_ACCEPTED = "|gameid|";
    private static String REGEX_ACROMAGE_GUILD_LEVEL = "gamblers.*?([\\d]+).*?([\\d]+).*?\\+([\\d]+)";
    private static String REGEX_INVITATION_LIST = "pl_info.*?<i>(.*?)\\s";
    private static String IS_ID = "[^\\d]+";

    private static String SWF_URL = "swffiles\\/workcode_en.swf\\?ver=[^\"]*";
    protected static String REGEX_FORM = "<param name=\"FlashVars\" value=\"params=([\\d]*)\\|([\\d]*)\\|([\\d\\w]*)\\|\"";
    protected static String HTML_OBJECT_ID = "<object id=\"(.+?)\".+?\\n.+?swffiles\\/workcode_en\\.swf";

    public static String getTitle(String body) {
        Matcher m = Pattern.compile(REGEX_TITLE).matcher(body.toLowerCase());
        String result = "No title";
        if (m.find()) {
            return m.group(1);
        }
        return result;
    }

    public static String getUserId(String body) {
        Matcher m = Pattern.compile(REGEX_USER_ID).matcher(body.toLowerCase());
        String result = BuildConfig.FLAVOR;
        if (m.find()) {
            return m.group(1);
        }
        return result;
    }

    public static String getWarId(String url) {
        Matcher m = Pattern.compile(REGEX_WAR_ID).matcher(url.toLowerCase());
        String result = BuildConfig.FLAVOR;
        if (m.find()) {
            return m.group(1);
        }
        return result;
    }

    public static String getGameId(String url) {
        Matcher m = Pattern.compile(REGEX_GAME_ID).matcher(url.toLowerCase());
        String result = BuildConfig.FLAVOR;
        if (m.find()) {
            return m.group(1);
        }
        return result;
    }

    public static boolean isGameAccepted(String tavern) {
        return tavern.toLowerCase().contains(IS_ACCEPTED);
    }

    public static String[] getAcromageLevel(String body) {
        Matcher m = Pattern.compile(REGEX_ACROMAGE_GUILD_LEVEL).matcher(body.toLowerCase());
        String[] data = null;
        if (m.find()) {
            data = new String[3];
            data[0] = m.group(1);
            data[1] = m.group(2);
            data[2] = m.group(3);
            return data;
        }
        return data;
    }

    public static String getUserLevel(String body) {
        Matcher m = Pattern.compile(REGEX_USER_LEVEL).matcher(body.toLowerCase());
        String result = BuildConfig.FLAVOR;
        if (m.find()) {
            return m.group(1);
        }
        return result;
    }

    public static String getUserExp(String body) {
        Matcher m = Pattern.compile(REGEX_USER_EXP).matcher(body.toLowerCase());
        String result = BuildConfig.FLAVOR;
        if (m.find()) {
            return m.group(3);
        }
        return result;
    }

    public static String getUserExpToLevel(String body) {
        Matcher m = Pattern.compile(REGEX_USER_EXP_TO_NEXT_LEVEL).matcher(body.toLowerCase());
        String result = BuildConfig.FLAVOR;
        if (m.find()) {
            return m.group(1);
        }
        return result;
    }

    public static String getUserGold(String body) {
        Matcher m = Pattern.compile(REGEX_USER_GOLD).matcher(body.toLowerCase());
        String result = BuildConfig.FLAVOR;
        if (m.find()) {
            return m.group(1);
        }
        return result;
    }

    public static String getRegisterCode(String body) {
        Matcher m = Pattern.compile(REGEX_REGISTER_CODE).matcher(body.toLowerCase());
        String result = BuildConfig.FLAVOR;
        if (m.find()) {
            return m.group(1);
        }
        return result;
    }

    public static String getIdForJoin(String body, final String user) {
        if (body.toLowerCase().contains(user.toLowerCase())) {
            body = body.substring(body.indexOf(user));
        }
        Matcher m = Pattern.compile(user + REGEX_JOIN_ID).matcher(body.toLowerCase());
        String result = BuildConfig.FLAVOR;
        if (m.find()) {
            if (m.group(3) != null && !TextUtils.isEmpty(m.group(1))
                    && !TextUtils.isEmpty(m.group(3))
                    && !m.group(1).toLowerCase().contains(PLAYER_IDENTIFCATOR)) {
                result = m.group(3);
            }
        }
        return result;
    }

    public static boolean isID(String name) {
        Matcher m = Pattern.compile(IS_ID).matcher(name);
        return !m.find();
    }

    public static boolean canAccept(String body) {
        return body.toLowerCase().contains(CAN_ACCEPT);
    }

    public static boolean waitingAccept(String body) {
        //TODO check correct string
        return body.toLowerCase().contains(WAITING_ACCEPT);
    }

    public static String[] getOpponentData(String body) {
        String[] data = null;
        Matcher m = Pattern.compile(REGEX_ACCEPT_USERNAME).matcher(body.toLowerCase());
        String result = BuildConfig.FLAVOR;
        if (m.find()) {
            data = new String[2];
            data[0] = m.group(1);
            data[1] = m.group(2);
        }
        return data;
    }

    public static List<String> getInvitationList(String body) {
        List<String> res = new ArrayList<>();
        Matcher m = Pattern.compile(REGEX_INVITATION_LIST).matcher(body);
        while (m.find()) {
            res.add(m.group(1));
        }
        return res;
    }

    public static String getSwfUrl(String body) {
        Matcher m = Pattern.compile(SWF_URL).matcher(body.toLowerCase());
        String result = BuildConfig.FLAVOR;
        if (m.find()) {
            return m.group(0);
        }
        return result;
    }

    public static String getSwfCodeId(String body) {
        Matcher m = Pattern.compile(REGEX_FORM).matcher(body);
        if (m.groupCount() > 0) {
            m.find(0);
            return m.group(3);
        }
        return BuildConfig.FLAVOR;
    }

    public static String getObjectId(String object) throws IOException {
        String result = "";
        Matcher m = Pattern.compile("object-info\\.php\\?id=([\\d]*)").matcher(object);
        if (m.find()) {
            result = m.group(1);
        }
        return result;
    }

    public static String getHtmlObjectId(String body) {
        String result = "";
        Matcher m = Pattern.compile(HTML_OBJECT_ID).matcher(body);
        if (m.find()) {
            result = m.group(1);
        }
        return result;
    }
}
