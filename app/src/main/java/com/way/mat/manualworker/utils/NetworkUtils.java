package com.way.mat.manualworker.utils;

import android.content.Context;
import android.net.ConnectivityManager;

public class NetworkUtils {

    public static boolean isConnected(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager.getActiveNetworkInfo() != null) {
            return (manager.getActiveNetworkInfo().isAvailable() && manager
                    .getActiveNetworkInfo().isConnected());
        }
        return false;
    }

}
