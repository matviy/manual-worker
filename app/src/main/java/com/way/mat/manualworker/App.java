package com.way.mat.manualworker;

import android.app.Application;
import android.content.pm.PackageManager;

import com.way.mat.manualworker.utils.PreferencesManager;


/**
 * Created by mpodolsky on 09.10.2015.
 */
public class App extends Application {

    private static App instance;

    public App() {
        instance = this;
    }

    public static App getInstance() {
        return instance;
    }

    public void onCreate() {
        super.onCreate();

        PreferencesManager.initializeInstance(this);
    }

    public String getVersionName() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return BuildConfig.FLAVOR;
        }
    }

}
