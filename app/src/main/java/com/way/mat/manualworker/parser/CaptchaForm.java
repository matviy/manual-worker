package com.way.mat.manualworker.parser;


import com.way.mat.manualworker.exceptions.CaptchaNotFoundException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CaptchaForm {
    protected static String REGEX_FORM = "<param name=\"FlashVars\" value=\"params=([\\d]*)\\|([\\d]*)\\|([\\d\\w]*)\\|\"";
    protected static String REGEX_IMG_SRC = "<img src='([^\"]*)' width=250 height=60 border=0 name=imgcode>";
//    protected static String REGEX_SWF_SRC_RU = "(swffiles/[\\d]+wworkcode.swf\\?ver=.*?)\"";
    protected static String REGEX_SWF_SRC = "(swffiles\\/workcode_en.swf\\?ver=.*?)\\\"";
    public String code;
    public String code_id;
    public String id;
    public Boolean is_workaholic = Boolean.valueOf(false);
    public String pl_id;
    public String src;
    public String body;
    public String swf;

    public static CaptchaForm create(String responseBody) throws CaptchaNotFoundException {
        CaptchaForm captchaForm = new CaptchaForm();
        captchaForm.src = getImgUrl(responseBody);
        captchaForm.swf = getSwfUrl(responseBody);
        captchaForm.body = responseBody;
        fillForm(captchaForm, responseBody);
        if (responseBody.contains("workaholic penalty enabled")) {
            captchaForm.is_workaholic = Boolean.valueOf(true);
        }
        return captchaForm;
    }

    protected static String getImgUrl(String form) throws CaptchaNotFoundException {
        Matcher m = Pattern.compile(REGEX_IMG_SRC).matcher(form);
        try {
            if (m.groupCount() > 0) {
                m.find(0);
                return m.group(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new CaptchaNotFoundException();
    }

    protected static String getSwfUrl(String form) throws CaptchaNotFoundException {
        Matcher m = Pattern.compile(REGEX_SWF_SRC).matcher(form);
        try {
            if (m.find()) {
                return m.group(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new CaptchaNotFoundException();
    }

    protected static void fillForm(CaptchaForm captchaForm, String form) throws CaptchaNotFoundException {
        Matcher m = Pattern.compile(REGEX_FORM).matcher(form);
        if (m.groupCount() > 0) {
            m.find(0);
            captchaForm.pl_id = m.group(1);
            captchaForm.id = m.group(2);
            captchaForm.code_id = m.group(3);
            return;
        }
        throw new CaptchaNotFoundException();
    }

    public String getCode() {
        return code;
    }

    public String getCode_id() {
        return code_id;
    }

    public String getId() {
        return id;
    }

    public Boolean getIs_workaholic() {
        return is_workaholic;
    }

    public String getPl_id() {
        return pl_id;
    }

    public String getSrc() {
        return src;
    }

    public String getBody() {
        return body;
    }

    public String getSwf() {
        return swf;
    }
}
